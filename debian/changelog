libbio-samtools-perl (1.43-5) unstable; urgency=medium

  * Team upload.

  [ Niko Tyni ]
  * Add missing prototypes
    Closes: #1065759

  [ Andreas Tille ]
  * Remove trailing whitespace in debian/changelog (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 12 Mar 2024 22:11:21 +0100

libbio-samtools-perl (1.43-4) unstable; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * Use nocheck profile instead of comment

  [ Étienne Mollier ]
  * [7239813] Declare compliance with Debian Policy 4.6.2.
  * [18528be] Enable all hardening flags.
  * [e28e17e] d/clean: new: cleanup test artifacts. (Closes: #1046631)
  * [8a5dc01] d/lintian-overrides: refresh mismatched override.
  * [a514974] typo.patch: fix a typo caught by lintian.

 -- Étienne Mollier <emollier@debian.org>  Thu, 21 Dec 2023 21:38:17 +0100

libbio-samtools-perl (1.43-3) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Andreas Tille ]
  * Remove deprecated fields from upstream metadata
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 07 Jun 2020 09:23:40 +0200

libbio-samtools-perl (1.43-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Nathan Handler from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ Andreas Tille ]
  * debhelper 12
  * Standards-Version: 4.3.0
  * Remove trailing whitespace in debian/copyright
  * Lintian-override for script-with-language-extension since this is
    agreement in Debian Med team

 -- Andreas Tille <tille@debian.org>  Tue, 05 Feb 2019 11:59:42 +0100

libbio-samtools-perl (1.43-1) unstable; urgency=medium

  [ Charles Plessy ]
  dd47de5 Imported Upstream version 1.43

  [ gregor herrmann ]
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.
  * New upstream release.
  * Update years of upstream copyright and licensing terms.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

 -- Charles Plessy <plessy@debian.org>  Sun, 21 Feb 2016 13:52:00 +0900

libbio-samtools-perl (1.41-1) unstable; urgency=medium

  0b7140a Imported Upstream version 1.41

 -- Charles Plessy <plessy@debian.org>  Thu, 18 Jun 2015 08:01:22 +0900

libbio-samtools-perl (1.39-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Rename debian/upstream to debian/upstream/metadata.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add build dependency on libmodule-build-perl.
    (Closes: #787670)
  * Declare compliance with Debian Policy 3.9.6.
  * Bump required debhelper version to 9.20120312~.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Thu, 04 Jun 2015 11:42:42 +0200

libbio-samtools-perl (1.39-1) unstable; urgency=low

  cff6561 Imported Upstream version 1.39

 -- Charles Plessy <plessy@debian.org>  Sun, 29 Sep 2013 08:37:36 +0900

libbio-samtools-perl (1.38-1) unstable; urgency=low

  715e2d1 Imported Upstream version 1.38

  [ Salvatore Bonaccorso ]
  6c4f50e Change search.cpan.org based URIs to metacpan.org based URIs

  [ Charles Plessy ]
  9cf0347 Use Debhelper 9.
  c747b89 Moved README.Debian to README.test as it is all about tests.

 -- Charles Plessy <plessy@debian.org>  Fri, 17 May 2013 12:38:01 +0900

libbio-samtools-perl (1.37-1) unstable; urgency=low

  183cfbc Imported Upstream version 1.37

  [ Nathan Handler ]
  d734fbe Email change: Nathan Handler -> nhandler@debian.org

  [ Charles Plessy ]
  c6aaf4a Conforms with Policy version 3.9.4.
  7dd10a2 Normalised VCS URLs according to lintian.

 -- Charles Plessy <plessy@debian.org>  Sun, 06 Jan 2013 09:04:28 +0900

libbio-samtools-perl (1.36-1) unstable; urgency=low

  [ Charles Plessy ]
  * [abf3eaa] Imported Upstream version 1.36
  * [d30ef61] Normalised Debian copyright file with 'cme fix dpkg-copyright'.
  * [6f6c6f3] Summarised License of the whole package in the copyright file.
  * [d5182f1] Conforms to Debian Policy version 3.9.3.

  [ gregor herrmann ]
  * [c3db6d4] Update {versioned,alternative} (build) dependencies.

 -- Charles Plessy <plessy@debian.org>  Sun, 09 Sep 2012 10:02:55 +0900

libbio-samtools-perl (1.33-1) unstable; urgency=low

  * [5fd8406] New upstream release 1.33

  [ Nathan Handler ]
  * [4058f51] Add myself to Uploaders and debian/copyright
  * [01985a1] Remove version from perl Build-Depend. oldstable has 5.10
  * [5b63fda] Don't Build-Depend on libmodule-build-perl (included in
    core since 5.10)
  * [af3530d] Remove version from libbio-perl-perl Build-Depend. Nothing
    older in archive
  * [d6d6dca] Don't Depend on perl, ${perl:Depends} will pull it in
  * [64ad8db] Turn short description into noun phrase based on abstract
    in upstream META.json

  [ Charles Plessy ]
  * [bb4b659] Renamed debian/upstream-metadata.yaml to debian/upstream.

 -- Charles Plessy <plessy@debian.org>  Sun, 29 Jan 2012 14:38:27 +0900

libbio-samtools-perl (1.32-1) unstable; urgency=low

  [36ac64b] New upstream release.
  [dfb9312] Depend on BioPerl 1.6.901, following Upstream.

 -- Charles Plessy <plessy@debian.org>  Wed, 23 Nov 2011 16:26:32 +0900

libbio-samtools-perl (1.31-1) unstable; urgency=low

  [abb8487] New upstream release 1.31
  [e222e9f] New upstream release 1.30
  [5d83169] Normalised debian/copyright by loading and saving it with
            config-edit.
  [de38fdb] Include 7 digits of the commit id in the changelog enty.

 -- Charles Plessy <plessy@debian.org>  Tue, 01 Nov 2011 19:22:49 +0900

libbio-samtools-perl (1.29-1) unstable; urgency=low

  [ Charles Plessy ]
  * (Build)-Depend on libbio-perl-perl instead of bioperl (debian/control).

  [ Maximilian Gass ]
  * New upstream release
  * Switch to source format 3.0 (quilt)
  * Format Depends in debian/control
  * Remove debian/libbio-samtools-perl.docs to fix duplicate changelog
  * Update debian/copyright
    * Add copyright assignment to Charles Plessy for debian/*
    * Replace inline Artistic-2.0 with properly wrapped version from
      upstream's LICENSE file
    * Merge Comment into body of GPL-1+
    * Remove unused Artistic License section

 -- Charles Plessy <plessy@debian.org>  Tue, 28 Jun 2011 13:26:10 +0900

libbio-samtools-perl (1.28-2) unstable; urgency=low

  * libbam-dev’s bam.h file moved to ‘/usr/include/samtools’.
    - Build-depend on libbam-dev >= 0.1.16-1~ (debian/control).
    - Override dh_auto_configure to indicate path (debian/rules).
    - Use Debhelper 8 (debian/control, debian/compat).
  * Incremented Standards-Version to reflect conformance with Policy 3.9.2
    (debian/control, no changes needed).
  * Validated debian/copyright with ‘config-edit -application dpkg-copyright’.

 -- Charles Plessy <plessy@debian.org>  Fri, 20 May 2011 15:02:02 +0900

libbio-samtools-perl (1.28-1) unstable; urgency=low

  * New upstream release.

 -- Charles Plessy <plessy@debian.org>  Mon, 04 Apr 2011 20:45:31 +0900

libbio-samtools-perl (1.27-1) unstable; urgency=low

  * Requires libbam 0.1.9 or superior (debian/control).
  * Conformance with Policy 3.9.1:
    - Point to GPL-1 instead of GPL in debian/copyright.
    - Incremented Standards-Version in debian/control.
  * Reformatted to debian/copyright DEP-5 format.

 -- Charles Plessy <plessy@debian.org>  Tue, 08 Mar 2011 20:09:10 +0900

libbio-samtools-perl (1.20-1) unstable; urgency=low

  * New upstream release.
  * Incremented Standards-Version to reflect conformance with Policy 3.9.0
    (debian/control, no changes needed).

 -- Charles Plessy <plessy@debian.org>  Sat, 17 Jul 2010 10:44:34 +0900

libbio-samtools-perl (1.19-1) unstable; urgency=low

  * New upstream release.

 -- Charles Plessy <plessy@debian.org>  Mon, 24 May 2010 16:25:08 +0900

libbio-samtools-perl (1.14-1) unstable; urgency=low

  * New upstream release with a new script, bamToGBrowse.pl.
  * Depend and Build-Depend on bioperl >= 1.6 (debian/control).
  * t/data/ex1.bam.bai and t/data/ex1.fa.fai are now part of the
    upstream source, so we do not delete them any more in the
    clean target of debian/rules.

 -- Charles Plessy <plessy@debian.org>  Tue, 06 Apr 2010 13:41:17 +0900

libbio-samtools-perl (1.12-1) unstable; urgency=low

  * New upstream release.

 -- Charles Plessy <plessy@debian.org>  Fri, 19 Mar 2010 22:46:54 +0900

libbio-samtools-perl (1.11-1) unstable; urgency=low

  * New upstream release.

 -- Charles Plessy <plessy@debian.org>  Sat, 06 Mar 2010 20:29:02 +0900

libbio-samtools-perl (1.10-1) unstable; urgency=low

  * New Upstream version.
  * Incremented Standards-Version to reflect conformance with Policy 3.8.4
    (debian/control, no changes needed).

 -- Charles Plessy <plessy@debian.org>  Thu, 11 Feb 2010 23:21:58 +0900

libbio-samtools-perl (1.09-1) unstable; urgency=low

  * New Upstream Versions adding support for remote access to BAM
    databases on http/ftp servers.

 -- Charles Plessy <plessy@debian.org>  Fri, 11 Dec 2009 22:53:34 +0900

libbio-samtools-perl (1.07-1) unstable; urgency=low

  * New upstream release.
    - IMPORTANT API CHANGE 1: The feature strand now returns +1 or -1 to
      indicate whether the query was reverse complemented in the SAM file.
    - IMPORTANT API CHANGE 2: The $feature->query object's dna() and seq()
      methods now return the sequence as it was read, rather than the reverse
      complemented version as represented in the SAM file. $feature->qseq()
      returns the reverse complemented version as before.
  * Removed mention that libbio-samtools-perl is built against a static
    library since it was agree with the release team that the package
    can enter in Testing.

 -- Charles Plessy <plessy@debian.org>  Mon, 23 Nov 2009 17:20:21 +0900

libbio-samtools-perl (1.06-1) unstable; urgency=low

  * New Upstream Version (Bio::DB::Sam modified to work with GBrowse 1).
  * Added a few informations about upstream in debian/upstream-metadata.yaml.
  * Documented how I test the package in README.Debian.

 -- Charles Plessy <plessy@debian.org>  Sun, 08 Nov 2009 11:43:43 +0900

libbio-samtools-perl (1.05-1) unstable; urgency=low

  * New upstream release.
    - Bio::DB::Sam->seq_id() method no longer lower-cases reference names.
    - Quashed enormous memory leak in the pileup() function.

 -- Charles Plessy <plessy@debian.org>  Mon, 28 Sep 2009 12:58:26 +0900

libbio-samtools-perl (1.04-1) unstable; urgency=low

  * Initial Release (Closes: #543487).

 -- Charles Plessy <plessy@debian.org>  Tue, 15 Sep 2009 18:55:53 +0900
