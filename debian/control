Source: libbio-samtools-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Charles Plessy <plessy@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libbam-dev,
               libbio-perl-perl <!nocheck>,
               libmodule-build-perl,
               perl-xs-dev,
               perl:native,
               zlib1g-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libbio-samtools-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libbio-samtools-perl.git
Homepage: https://metacpan.org/release/Bio-SamTools
Rules-Requires-Root: no

Package: libbio-samtools-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends},
         libbio-perl-perl
Enhances: gbrowse,
          samtools
Description: Perl interface to SamTools library for DNA sequencing
 Bio::SamTools provides a Perl interface to the libbam library for indexed and
 unindexed SAM/BAM sequence alignment databases. It provides support for
 retrieving information on individual alignments, read pairs, and alignment
 coverage information across large regions. It also provides callback
 functionality for calling SNPs and performing other base-by-base functions.
 Most operations are compatible with the BioPerl Bio::SeqFeatureI interface,
 allowing BAM files to be used as a backend to the GBrowse genome browser
 application.
